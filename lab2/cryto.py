import random
from keygenerators import (solitare, blum_blum_shub)

def encrypt(plaintext, keygenerator, key):
    n = len(plaintext)
    (code, new_key) = keygenerator(key, n)

    # plaintext = bytes(plaintext, 'utf-8')

    list_ciphertext = []
    for i in range(n):
        list_ciphertext.append(plaintext[i] ^ code[i])
    
    ciphertext = bytes(list_ciphertext)
    return (ciphertext, new_key)

def decrypt(ciphertext, keygenerator, key):
    n = len(ciphertext)
    (code, new_key) = keygenerator(key, n)

    list_plaintext = []
    for i in range(n):
        list_plaintext.append(ciphertext[i] ^ code[i])
    
    plaintext = bytes(list_plaintext)
    # plaintext = plaintext.decode("utf-8")
    return (plaintext, new_key)

if __name__ == '__main__':
    key = [i+1 for i in range(54)]
    random.shuffle(key)
    print(key)

    (cipher, new_key) = encrypt(b"Szia mia mizu", solitare, key)
    print(key)

    (plaintext, new_key) = decrypt(cipher, solitare, key)
    print(new_key)
    print(plaintext)

    key = [234,100]

    (cipher, new_key) = encrypt(b"Szia mia mizu megint", blum_blum_shub, key)
    print(key)

    (plaintext, new_key) = decrypt(cipher, blum_blum_shub, key)
    print(new_key)
    print(plaintext)





    