from socket import *
import threading
from numpy import *
from keygenerators import (solitare, blum_blum_shub)
from cryto import (encrypt, decrypt)
from config import (solitare_key, blum_blum_shub_key)

global key
key = solitare_key.copy()
# key = blum_blum_shub_key.copy()
print("If something is not clear, just type: help")
serverName = 'localhost'
serverPort=12000
clientSocket=socket(AF_INET,SOCK_STREAM)
clientSocket.connect((serverName,serverPort))



def listen(clientSocket):
    global key
    while True:
        cipher=clientSocket.recv(1024)
        (plaintext, new_key) = decrypt(cipher, solitare, key)
        # (plaintext, new_key) = decrypt(cipher, blum_blum_shub, key)
        key = new_key.copy()
        print(plaintext.decode("utf-8"))
        
        

def send(clientSocket):
    global key
    while True:
        text=input()
        byte_text = bytes(text, 'utf-8')
        if True:
            (cipher, new_key) = encrypt(byte_text, solitare, key)
            # (cipher, new_key) = encrypt(byte_text, blum_blum_shub, key)
            key = new_key.copy()
            clientSocket.send(cipher)



try:
   x = threading.Thread(target=send, args=(clientSocket,))
   x.start()
   x = threading.Thread(target=listen, args=(clientSocket,))
   x.start()
except:
   print ("Error: unable to start thread")
