#!/usr/bin/env python3 -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Course: CS 41
Name: <YOUR NAME>
SUNet: <SUNet ID>

Replace this with a description of the program.
"""
import utils

# Caesar Cipher

def encrypt_caesar(plaintext, binary):
    """Encrypt plaintext using a Caesar cipher.

    Add more implementation details here.
    In order to encrypt the input, I calculated the value of each letter (A = 0, B = 1, ...) 
    added 3 and replaced the letter with the corresponding letter of the new value.

    @param plaintext that contains the data that will be encryted and binary whether input is a string or a binary file
    @type plaintext string, binary bool

    @return encrypted result
    """
    # raise NotImplementedError  # Your implementation here
    if(len(plaintext) == 0 ):
        raise ValueError
    if plaintext != plaintext.upper() and not binary:
        plaintext = plaintext.upper()

    if binary:
        ciphertext = b''
    else:
        ciphertext = ''

    for letter in plaintext:
        if binary:
            ciphertext += bytes([(letter+3) % 256])
            # ciphertext += chr((letter+3)%256)
        else:
            if('A' <= letter and letter <= 'Z'):
                # ciphertext += chr((ord(letter) + 3) % 256)
                ciphertext += utils.convert_to_char((utils.convert_to_number(letter) + 3) % 26)
            else: 
                ciphertext += letter

    # if binary:
        # return ciphertext.encode('ansi', 'ignore')
    return ciphertext


def decrypt_caesar(ciphertext, binary):
    """Decrypt a ciphertext using a Caesar cipher.

    Add more implementation details here.
    While decrypting I did the same thing, but instead of adding three, I extracted.

    @param ciphertext that contains the data that will be decryted and binary whether input is a string or a binary file
    @type ciphertext string, binary bool

    @return decrypted result
    """
    # raise NotImplementedError  # Your implementation here
    if(len(ciphertext) == 0 ):
        raise ValueError
    if ciphertext != ciphertext.upper() and not binary:
        ciphertext = ciphertext.upper()

    if binary:
        plaintext = b''
    else:
        plaintext = ''

    for letter in ciphertext:
        if binary:
            plaintext += bytes([(letter-3) % 256])
            # plaintext += chr((letter-3)%256)
        else:
            if('A' <= letter and letter <= 'Z'):
                plaintext += utils.convert_to_char((utils.convert_to_number(letter) - 3) % 26)
            else:
                plaintext += letter

    # if binary:
    #     return bytes(plaintext, 'ansi')
    return plaintext


# Vigenere Cipher

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    Add more implementation details here.
    It is very similar to the caesar, but here I used a keyword, and its letters, 
    so this time I do not add 3 every single time, instead I add the value of the 
    current letter in the keyword.

    @param plaintext that contains the data that will be encryted and keyword
    @type plaintext string, keyword string

    @return decrypted result
    """
    # raise NotImplementedError  # Your implementation here
    if(len(plaintext) == 0 or len(keyword) == 0):
        raise ValueError
    if plaintext != plaintext.upper():
        plaintext = plaintext.upper()
    if keyword != keyword.upper():
        keyword = keyword.upper()

    n = len(plaintext)
    m = len(keyword)

    ciphertext = ''

    for i in range(n):
        ciphertext += utils.convert_to_char((utils.convert_to_number(plaintext[i]) + utils.convert_to_number(keyword[i%m])) % 26) 

    return ciphertext



def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    Add more implementation details here.
    While decrypting I did the same thing, but instead of adding three, I extracted.

    @param ciphertext that contains the data that will be decryted and keyword
    @type ciphertext string, keyword string

    @return decrypted result
    """
    # raise NotImplementedError  # Your implementation here
    if(len(ciphertext) == 0 or len(keyword) == 0):
        raise ValueError
    if ciphertext != ciphertext.upper():
        ciphertext = ciphertext.upper()
    if keyword != keyword.upper():
        keyword = keyword.upper()

    n = len(ciphertext)
    m = len(keyword)

    plaintext = ''

    for i in range(n):
        plaintext += utils.convert_to_char((utils.convert_to_number(ciphertext[i]) - utils.convert_to_number(keyword[i%m])) % 26) 

    return plaintext

def encrypt_scytale(plaintext, circumference):
    """Encrypt plaintext using a Scytale cipher with a circumference.

    Add more implementation details here.
    In order to encrypt I discovered a method to play with indexes. 
    I take the first k letters and jump k steps in the input data, 
    while I am concatenating the letters to the result.

    @param plaintext that contains the data that will be encryted and circumference that shows how many rows do we use
    @type plaintext string, circumference number

    @return decrypted result
    """
    if(len(plaintext) == 0):
        raise ValueError

    if plaintext != plaintext.upper():
        plaintext = plaintext.upper()

    ciphertext = ''
    circumference = int(circumference)

    print("ki",circumference)
    for i in range(circumference):
        ciphertext += plaintext[i]
        j=i+circumference
        while j<len(plaintext):
            ciphertext += plaintext[j]
            j = j + circumference

    return ciphertext

def decrypt_scytale(ciphertext, circumference):
    """Decrypt plaintext using a Scytale cipher with a circumference.

    Add more implementation details here.
    Before decrypting, I calculate (using the circumference) how many letters fits in a row and how 
    many rows will remain that are not complete. After that I am able to decrypt with jumping between 
    the indexes in a similar way.

    @param ciphertext that contains the data that will be decryted and circumference that shows how many rows do we use
    @type ciphertext string, circumference number

    @return encrypted result
    """

    if(len(ciphertext) == 0):
        raise ValueError

    if ciphertext != ciphertext.upper():
        ciphertext = ciphertext.upper()

    plaintext = ''
    circumference = int(circumference)

    k = len(ciphertext) // circumference
    m = len(ciphertext) % circumference
    print (k, m)
    if(m>0):
        border = k+1
    else:
        border = k

    for i in range(border):
        if len(plaintext) < len(ciphertext):
            plaintext += ciphertext[i]
        szamlalo = 0
        if szamlalo<m:
                j = i + k + 1
        else:
                j = i + k
        # j=i+k

        while j<len(ciphertext):
            if len(plaintext) < len(ciphertext):
                plaintext += ciphertext[j]
            szamlalo += 1
            if szamlalo<m:
                j = j + k + 1
            else:
                j = j + k

    return plaintext

def encrypt_railfence(plaintext, num_rails):
    """Encrypt plaintext using a Railfence cipher with a num_rails.

    Add more implementation details here.
    In the end of the day I did not got enough brain cells to find out 
    a good method with the indexes like in the scytale cipher, so I just simply simulated the whole process.
    I stored the letters in a 2D array in zig-zag format and after that I concatenated the letters in the result.

    @param plaintext that contains the data that will be encryted and num_rails that shows how many rows do we use
    @type plaintext string, num_rails number

    @return decrypted result
    """
    if(len(plaintext) == 0):
        raise ValueError

    if plaintext != plaintext.upper():
        plaintext = plaintext.upper()

    ciphertext = ''
    num_rails = int(num_rails)

    
    matrix = [['.' for i in range(len(plaintext))]
                  for j in range(num_rails)]
    
    sor = 0
    oszlop = 0
     
    for i in range(len(plaintext)):
         
        if sor == 0:
            le = True
        if sor == num_rails - 1:
            le = False
         
        matrix[sor][oszlop] = plaintext[i]
        oszlop += 1
         
        if le:
            sor += 1
        else:
            sor -= 1

    for i in range(num_rails):
        for j in range(len(plaintext)):
            if matrix[i][j] != '.':
                ciphertext += matrix[i][j]
    
    return ciphertext

def decrypt_railfence(ciphertext, num_rails):
    """Decrypt plaintext using a Scytale cipher with a circumference.

    Add more implementation details here.

    @param ciphertext that contains the data that will be decryted and num_rails that shows how many rows do we use
    @type ciphertext string, num_rails number

    @return encrypted result
    """

    if(len(ciphertext) == 0):
        raise ValueError

    if ciphertext != ciphertext.upper():
        ciphertext = ciphertext.upper()

    plaintext = ''
    num_rails = int(num_rails)

    matrix = [['.' for i in range(len(ciphertext))]
                  for j in range(num_rails)]
     
    sor = 0
    oszlop = 0
     
    for i in range(len(ciphertext)):
        if sor == 0:
            le = True
        if sor == num_rails - 1:
            le = False
         
        matrix[sor][oszlop] = '*'
        oszlop += 1
         
        if le:
            sor += 1
        else:
            sor -= 1
             
    index = 0
    for i in range(num_rails):
        for j in range(len(ciphertext)):
            if ((matrix[i][j] == '*') and (index < len(ciphertext))):
                matrix[i][j] = ciphertext[index]
                index += 1
         
    sor = 0 
    oszlop = 0
    for i in range(len(ciphertext)):
         
        if sor == 0:
            le = True
        if sor == num_rails-1:
            le = False
             
        if (matrix[sor][oszlop] != '*'):
            plaintext += matrix[sor][oszlop]
            oszlop += 1
             
        if le:
            sor += 1
        else:
            sor -= 1
    
    return plaintext



# Merkle-Hellman Knapsack Cryptosystem

def generate_private_key(n=8):
    """Generate a private key for use in the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key components
    of the MH Cryptosystem. This consistutes 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        (Note: you can check if a sequence is superincreasing with `utils.is_superincreasing(seq)`)
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q` (you can use utils.coprime)

    You'll need to use the random module for this function, which has been imported already

    Somehow, you'll have to return all of these values out of this function! Can we do that in Python?!

    @param n bitsize of message to send (default 8)
    @type n int

    @return 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    raise NotImplementedError  # Your implementation here

def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one line using a list comprehension

    @param private_key The private key
    @type private_key 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    @return n-tuple public key
    """
    raise NotImplementedError  # Your implementation here


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    1. Separate the message into chunks the size of the public key (in our case, fixed at 8)
    2. For each byte, determine the 8 bits (the `a_i`s) using `utils.byte_to_bits`
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk in the message

    Hint: think about using `zip` at some point

    @param message The message to be encrypted
    @type message bytes
    @param public_key The public key of the desired recipient
    @type public_key n-tuple of ints

    @return list of ints representing encrypted bytes
    """
    raise NotImplementedError  # Your implementation here

def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key

    1. Extract w, q, and r from the private key
    2. Compute s, the modular inverse of r mod q, using the
        Extended Euclidean algorithm (implemented at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum using c' and w to recover the original byte
    5. Reconsitite the encrypted bytes to get the original message back

    @param message Encrypted message chunks
    @type message list of ints
    @param private_key The private key of the recipient
    @type private_key 3-tuple of w, q, and r

    @return bytearray or str of decrypted characters
    """
    raise NotImplementedError  # Your implementation here

