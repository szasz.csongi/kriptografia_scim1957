import random
import utils

def encrypt(plaintext, keygenerator, key):
    n = len(plaintext)
    (code, new_key) = keygenerator(key, n)

    # plaintext = bytes(plaintext, 'utf-8')

    list_ciphertext = []
    for i in range(n):
        list_ciphertext.append(plaintext[i] ^ code[i])
    
    ciphertext = bytes(list_ciphertext)
    return (ciphertext, new_key)

def decrypt(ciphertext, keygenerator, key):
    n = len(ciphertext)
    (code, new_key) = keygenerator(key, n)

    list_plaintext = []
    for i in range(n):
        list_plaintext.append(ciphertext[i] ^ code[i])
    
    plaintext = bytes(list_plaintext)
    # plaintext = plaintext.decode("utf-8")
    return (plaintext, new_key)


#-------------------------------------------------------------------------------

def generate_private_key(n=8):
    """Generate a private key for use in the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key components
    of the MH Cryptosystem. This consistutes 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        (Note: you can check if a sequence is superincreasing with `utils.is_superincreasing(seq)`)
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q` (you can use utils.coprime)

    You'll need to use the random module for this function, which has been imported already

    Somehow, you'll have to return all of these values out of this function! Can we do that in Python?!

    @param n bitsize of message to send (default 8)
    @type n int

    @return 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    w = []
    nr = random.randint(2,10)
    w.append(nr)
    sum = nr

    for i in range(n-1):
        nr = random.randint(sum+1, 2*sum)
        w.append(nr)
        sum += nr
    
    q = random.randint(sum+1, 2*sum)

    r = random.randint(2, q-1)
    while (not utils.is_coprime(q,r)):
        r = random.randint(2, q-1)

    return(w, q, r)

def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one line using a list comprehension

    @param private_key The private key
    @type private_key 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    @return n-tuple public key
    """
    w,q,r = private_key

    return [r * item % q for item in w]

def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    1. Separate the message into chunks the size of the public key (in our case, fixed at 8)
    2. For each byte, determine the 8 bits (the `a_i`s) using `utils.byte_to_bits`
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk in the message

    Hint: think about using `zip` at some point

    @param message The message to be encrypted
    @type message bytes
    @param public_key The public key of the desired recipient
    @type public_key n-tuple of ints

    @return list of ints representing encrypted bytes
    """
    cipher = []
    for char in message:
        a = utils.char_to_bits(char)
        cipher.append(sum(a[i] * public_key[i] for i in range(8)))
    return cipher

def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key

    1. Extract w, q, and r from the private key
    2. Compute s, the modular inverse of r mod q, using the
        Extended Euclidean algorithm (implemented at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum using c' and w to recover the original byte
    5. Reconsitite the encrypted bytes to get the original message back

    @param message Encrypted message chunks
    @type message list of ints
    @param private_key The private key of the recipient
    @type private_key 3-tuple of w, q, and r

    @return bytearray or str of decrypted characters
    """
    w, q, r = private_key

    s = utils.modinv(r,q)

    result = []
    c_ = []

    for item in message:
        c_.append(item*s%q)

    for char in c_:
        bits = []

        for item in w[::-1]:
            if char >= item:
                bits.append(1)
                char -= item
            else:
                bits.append(0)

        result.append(utils.bits_to_char(bits[::-1]))
    return "".join(result)


if __name__ == '__main__':
    private_key = generate_private_key(8)
    public_key = create_public_key(private_key)

    message = encrypt_mh('Abma12 kutya mcsaka 1243 32', public_key)
    print(message)
    print(decrypt_mh(message, private_key))



    

