from socket import *
import threading
from numpy import *
from keygenerators import (solitare)
from crypto import (decrypt_mh, encrypt_mh, decrypt, generate_private_key, create_public_key, encrypt, decrypt)
import utils
import random

def listen(clientSocket):
    global key
    while True:
        cipher=clientSocket.recv(1024)
        (plaintext, new_key) = decrypt(cipher, solitare, key)
        # (plaintext, new_key) = decrypt(cipher, blum_blum_shub, key)
        key = new_key.copy()
        print(plaintext.decode("utf-8"))
        
        

def send(clientSocket):
    global key
    while True:
        text=input()
        byte_text = bytes(text, 'utf-8')
        if True:
            (cipher, new_key) = encrypt(byte_text, solitare, key)
            # (cipher, new_key) = encrypt(byte_text, blum_blum_shub, key)
            key = new_key.copy()
            clientSocket.send(cipher)

if __name__ == '__main__':
    private_key = generate_private_key(8)
    public_key = create_public_key(private_key)

    serverName = 'localhost'
    serverPort=8000
    clientSocket=socket(AF_INET,SOCK_STREAM)
    clientSocket.connect((serverName,serverPort))

    
    clientSocket.send(b'8002')
    print("ID2 sent")
    b_public_key = utils.list_to_bitstring(public_key)
    clientSocket.send(b_public_key)
    print("Punblic key 2 sent")

    target_id = input("To whom you want to write: ")
    b_target_id = bytes(target_id, encoding='utf8')
    clientSocket.send(b_target_id)
    print("Target ID sent")

    b_target_public_key = clientSocket.recv(1024)
    target_public_key = utils.bitstring_to_list(b_target_public_key)
    print("Target publuc key: ", target_public_key)

    #Recieving Hello from target
    recvPort=8002
    recvSocket=socket(AF_INET,SOCK_STREAM)
    recvSocket.bind(('',recvPort))
    recvSocket.listen(1)
    connectionSocket,addr = recvSocket.accept()
    b_message = connectionSocket.recv(1024)
    encrypted_message = utils.bitstring_to_list(b_message)
    message = decrypt_mh(encrypted_message, private_key)

    print("Hello: ", message)

    #Send Hello for taget
    target_id = int(target_id)
    targetSocket = socket(AF_INET,SOCK_STREAM)
    targetSocket.connect((serverName,target_id))

    message = encrypt_mh('Hello, Szia', target_public_key)
    b_message = utils.list_to_bitstring(message)
    targetSocket.send(b_message)
    print("Hello sent")

    #Getting the half secret from client1
    b_encrypted_half_secret = connectionSocket.recv(1024)
    encrypted_half_secret = utils.bitstring_to_list(b_encrypted_half_secret)
    string_half_secret = decrypt_mh(encrypted_half_secret, private_key)
    half_secret = utils.string_to_list(string_half_secret)
    print("Half secret from client1: ", half_secret)

    #Generating the other half of the secret
    solitare_key = half_secret.copy()
    for i in range(27):
        num = random.randint(1, 54)
        while (num in solitare_key):
            num = random.randint(1, 54)
        solitare_key.append(num)

    string_solitare_key = utils.list_to_string(solitare_key)
    encrypted_solitare_key = encrypt_mh(string_solitare_key, target_public_key)
    b_encrypted_solitare_key = utils.list_to_bitstring(encrypted_solitare_key)
    targetSocket.send(b_encrypted_solitare_key)
    print("The whole secret sent: ", solitare_key)


    global key
    key = solitare_key.copy()

    #Starting communication with solitare encryption
    x = threading.Thread(target=send, args=(targetSocket,))
    x.start()
    x = threading.Thread(target=listen, args=(connectionSocket,))
    x.start()

