from copy import deepcopy

def solitare(deck_copy, n):
    key = []
    deck = deepcopy(deck_copy)
    white = deck.index(53)
    black = deck.index(54)
    i=0
    while i < n:
        #a
        # print(deck)
        if white != 53:
            deck[white], deck[white + 1] = deck[white + 1], deck[white]
            white = white + 1
        else:
            deck[white], deck[1] = deck[1], deck[white]
            white = 1


        #b
        if black != 53 and black != 52:
            deck[black], deck[black + 2] = deck[black + 2], deck[black]
            black = black + 2
        elif black == 53:
            deck[black], deck[2] = deck[2], deck[black]
            black = 2
        else:
            deck[black], deck[1] = deck[1], deck[black]
            black = 1

        #c
        if white < black:
            first = white
            second = black
        else:
            first = black
            second = white

        deck = deck[second+1:] + deck[first:second+1] + deck[:first]
        white = deck.index(53)
        black = deck.index(54)

        #d
        last = deck[-1]
        if last < 53:
            deck = deck[last+1:-1] + deck[:last+1] + deck[-1:]
            white = deck.index(53)
            black = deck.index(54)

        #e
        first = deck[0]
        if first < 53:
            key.append(deck[first+1])
            i = i+1

    return (key, deck)

def blum_blum_shub(key, nr):
    p = 7043
    q = 4271
    n = p*q
    seed = key[0]
    offset = key[1]
    if(seed < 1 or seed > n):
        print("Wrong seed")
        return
    x = (seed ** 2) % n
    for i in range(offset):
        for j in range(8):
            x = (x ** 2) % n

    z = []
    for i in range(nr):
        byte = []
        for j in range(8):
            byte.append(x % 2)
            x = (x ** 2) % n  

        string_byte = map(str, byte)
        string = ''.join(string_byte)
        z.append(int(string, 2))

    return (z, key)





