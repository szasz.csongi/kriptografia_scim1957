def gcd(p,q):
    while q != 0:
        p, q = q, p%q
    return p
def is_coprime(x, y):
    return gcd(x, y) == 1
    
def char_to_bits(char):
    lanc = bin(ord(char)).replace('b','')
    if len(lanc) == 7:
        lanc = "0" + lanc
    return [int(item) for item in lanc]

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    g, y, x = egcd(b%a,a)
    return (g, x - (b//a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('No modular inverse')
    return x%m

def bits_to_char(bits):
    string = "".join(str(item) for item in bits)
    return (chr(int(string,2)))

def list_to_bitstring(list):
    string_ints = [str(int) for int in list]
    str_of_ints = ",".join(string_ints)
    bit_of_str = bytes(str_of_ints,encoding='utf8')
    return bit_of_str

def bitstring_to_list(bitstring):
    string = bitstring.decode("utf-8")
    li = list(string.split(","))
    li = [int(item) for item in li]
    return li

def list_to_string(list):
    string_ints = [str(int) for int in list]
    str_of_ints = ",".join(string_ints)
    return str_of_ints

def string_to_list(string):
    li = list(string.split(","))
    li = [int(item) for item in li]
    return li
